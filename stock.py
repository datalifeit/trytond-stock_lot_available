# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.transaction import Transaction
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.const import OPERATORS


class ShipmentOut(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    @classmethod
    def __setup__(cls):
        super(ShipmentOut, cls).__setup__()
        cls.outgoing_moves.context.update({
            'available_test': True
        })


class Lot(metaclass=PoolMeta):
    __name__ = 'stock.lot'

    available = fields.Function(fields.Boolean('Available'), 'get_available',
        searcher='search_available')

    def get_available(self, name=None):
        return self.quantity and self.quantity > 0

    @classmethod
    def search_available(cls, name, clause):
        assert clause[1] == '='
        operator = {
            True: '>',
            False: '<='
        }
        return [('quantity', operator[clause[2]], 0)]

    @classmethod
    def _search_domain_active(cls, domain, active_test=True):
        domain = super()._search_domain_active(domain, active_test)
        if not Transaction().context.get('available_test', False):
            return domain

        def process(domain):
            i = 0
            active_found = False
            while i < len(domain):
                arg = domain[i]
                # add test for xmlrpc that doesn't handle tuple
                if (isinstance(arg, tuple) or (isinstance(arg, list) and
                        len(arg) > 2 and arg[1] in OPERATORS)):
                    if arg[0] == 'available':
                        active_found = True
                elif isinstance(arg, list):
                    domain[i] = process(domain[i])
                i += 1
            if not active_found:
                if (domain and ((isinstance(domain[0], str) and
                        domain[0] == 'AND') or
                        (not isinstance(domain[0], str)))):
                    domain.append(('available', '=', True))
                else:
                    domain = ['AND', domain, ('available', '=', True)]
            return domain
        return process(domain)

    @classmethod
    def get_quantity(cls, lots, name):
        "Return null instead of 0.0 if no locations in context"
        if not Transaction().context.get('locations'):
            return {}.fromkeys([l.id for l in lots], None)
        return super().get_quantity(lots, name)


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.lot.context['locations'] = [Eval('from_location')]
        if 'from_location' not in cls.lot.depends:
            cls.lot.depends.append('from_location')
        cls.lot.loading = 'lazy'

        if 'product' not in cls.lot.depends:
            cls.lot.depends.append('product')
        cls.lot.states['readonly'] |= ~Eval('product') | ~Eval('from_location')
